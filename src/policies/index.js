'use strict';

const isAuthorizedAccess = require('./isAuthorizedAccess'),
  isAuthorizedService = require('./isAuthorizedService');

module.exports = { isAuthorizedAccess, isAuthorizedService };
