'use strict';

const config = require('../config'),
  jwtToken = require('../services/jwtService'),
  log = require('../helpers/log'),
  { errorObjects } = require('../constants/error');

const unauthorizeError = errorObjects.UNAUTHORIZED;
const authHeaderNotFoundError = { ...unauthorizeError, message: 'Authorization header not found.' };
const authHeaderFormatError = {
  ...unauthorizeError, message: 'Format is Authorization: Bearer [token]'
};

module.exports = (req, res, next) => {
  log.debug('policies.isAuthorizedService');

  if (!(req.headers && req.headers.authorization)) {
    log.debug('Authorize header not found');
    next(authHeaderNotFoundError);
    return Promise.resolve(authHeaderNotFoundError);
  }

  const [scheme, token] = req.headers.authorization.split(' ');

  if (!((/^Bearer$/ui).test(scheme) && token)) {
    log.debug('Authorize header format error');
    next(authHeaderFormatError);
    return Promise.resolve(authHeaderFormatError);
  }

  return jwtToken.verify(token, config.SERVICE_SECRET)
    .then((payload) => {
    // auth data format is depended on each service
    // other middleware following this middleware can access auth data by 'req.auth'
      req.auth = payload;
      return next();
    })
    .catch((err) => next({ ...unauthorizeError, message: err }));
};
