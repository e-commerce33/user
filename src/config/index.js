'use strict';

const path = require('path');

require('dotenv').config({ path: path.resolve(__dirname, '..', '..', '.env') });

const envDefault = {
  POST: 3000,
  LOG_LEVEL: 'debug',
  SERVICE_NAME: 'user'
};
const nodeEnv = (process.env.NODE_ENV || 'development').toLowerCase();

module.exports = {
  NODE_ENV: nodeEnv,
  PORT: Number(process.env.PORT) || envDefault.POST,
  LOG: { LEVEL: Number(process.env.LOG_LEVEL) || envDefault.LOG_LEVEL },
  ACCESS_SECRET: process.env.ACCESS_SECRET,
  SERVICE_NAME: process.env.SERVICE_NAME || envDefault.SERVICE_NAME,
  SERVICE_SECRET: process.env.SERVICE_SECRET,
  DATABASE: { CONNECTION_STRING: process.env.DATABASE_CONNECTION_STRING }
};
