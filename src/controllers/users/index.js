'use strict';

const Model = require('./model'),
  apiSchema = require('./apiSchema'),
  httpStatus = require('../../constants/httpStatus'),
  log = require('../../helpers/log');

function create(req, res, next) {
  log.debug('users.create');

  return apiSchema.create.body.validateAsync(req.body)
    .then((value) => Model.create(value))
    .then((result) => res.status(httpStatus.OK).json(result))
    .catch(next);
}

function getMe(req, res, next) {
  log.debug('users.getMe');

  const email = req.auth.email;

  return Model.findOne({ email })
    .then((result) => res.status(httpStatus.OK).json(result))
    .catch(next);
}

module.exports = { create, getMe };
