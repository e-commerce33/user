'use strict';

const mongoose = require('mongoose');

const mainDb = require('../../loaders/mongoose').getConnection();

const Schema = mongoose.Schema;

const userSchena = new Schema({
  email: {
    type: String,
    required: true,
    index: { unique: true }
  },
  firstname: {
    type: String,
    minlength: 1,
    maxlength: 50,
    default: null
  },
  lastname: {
    type: String,
    minlength: 1,
    maxlength: 50,
    default: null
  },
  gender: {
    type: String
  }
}, { timestamps: true });

module.exports = mainDb.model('User', userSchena, 'users');
