'use strict';

const joi = require('joi');

const schema = {
  id: joi.string().hex().length(24),
  email: joi.string().email().trim(),
  firstname: joi.string().min(1).max(50).trim(),
  lastname: joi.string().min(1).max(50).trim(),
  gender: joi.string().valid('male', 'famale')
};

module.exports = {
  create: {
    body: joi.object().keys({
      email: schema.email.required(),
      firstname: schema.firstname.required(),
      lastname: schema.lastname.required(),
      gender: schema.gender.required()
    })
  }
};
