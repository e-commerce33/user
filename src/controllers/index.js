'use strict';

const { combine } = require('../helpers/api'),
  users = require('./users');

const combined = combine({
  users
});

module.exports = combined;
