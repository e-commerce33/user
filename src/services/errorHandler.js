'use strict';

const httpStatus = require('../constants/httpStatus'),
  log = require('../helpers/log');

function errorHandler(err, req, res, next) {
  log.debug('error handler');

  // response is already sent => send to express default handler!!
  if (res.headersSent) {
    return next(err);
  }

  if (err.isCustom) {
    delete err.isCustom;
    return res.status(err.statusCode).json(err);
  }

  if (err.isJoi) {
    const obj = {
      statusCode: httpStatus.BAD_REQUEST,
      message: err.message
    };
    delete obj.isCustom;

    return res.status(httpStatus.BAD_REQUEST).json(obj);
  }

  return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
}

module.exports = errorHandler;
